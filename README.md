## General info

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

Images should be served from CDN server, but in this demo case I used local resources for simplicity.

I used json-server to mock real backend REST API.

## Run Application

First run: `npm install` to install all dependencies 

Next install json-server: `npm install -g json-server`

And run them: `json-server --watch ./mockData/movie.mock-data.json` to start REST API mock server

Server will be started on url: http://localhost:3000

The server mock backend REST API with movie's data.

At the end run `ng serve` for a dev server. Navigate to http://localhost:4200/.


### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
