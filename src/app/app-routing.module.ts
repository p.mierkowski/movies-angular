import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MovieListComponent} from './components/movie-list/movie-list.component';
import {MovieItemComponent} from './components/movie-item/movie-item.component';

const routes: Routes = [
  {path: '', component: MovieListComponent},
  {path: 'genre/:genre', component: MovieListComponent},
  {path: 'movie/:id/:key', component: MovieItemComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
