import {Action} from '@ngrx/store';
import {Movie} from '../../models/movie.model';
import {GenreType} from '../../models/genre-type.model';

export enum MovieListActionTypes {
  MOVIE_LIST_LOAD = '[MovieList] Load Movies',
  MOVIE_LIST_SUCCESS = '[MovieList] Load Movies Success',
  MOVIE_LIST_ERROR = '[MovieList] Load Movies Error'
}


export class MovieListLoad implements Action {
  readonly type = MovieListActionTypes.MOVIE_LIST_LOAD;

  constructor(public payload: { query: string, genre: GenreType }) {
  }
}

export class MovieListSuccess implements Action {
  readonly type = MovieListActionTypes.MOVIE_LIST_SUCCESS;

  constructor(public payload: { movies: Movie[] }) {
  }
}

export class MovieListError implements Action {
  readonly type = MovieListActionTypes.MOVIE_LIST_ERROR;

  constructor(public payload: { error: Error | null }) {
  }
}


export type MovieListActions = MovieListLoad | MovieListSuccess | MovieListError;
