import {Action} from '@ngrx/store';
import {Movie} from '../../models/movie.model';

export enum MovieItemActionTypes {
  MOVIE_ITEM_LOAD = '[MovieItem] Load Movie',
  MOVIE_ITEM_SUCCESS = '[MovieItem] Load Movie Success',
  MOVIE_ITEM_ERROR = '[MovieItem] Load Movie Error',
}


export class MovieItemLoad implements Action {
  readonly type = MovieItemActionTypes.MOVIE_ITEM_LOAD;

  constructor(public payload: { id: number }) {
  }
}

export class MovieItemSuccess implements Action {
  readonly type = MovieItemActionTypes.MOVIE_ITEM_SUCCESS;

  constructor(public payload: { movie: Movie }) {
  }
}

export class MovieItemError implements Action {
  readonly type = MovieItemActionTypes.MOVIE_ITEM_ERROR;

  constructor(public payload: { error: Error | null }) {
  }
}


export type MovieItemActions = MovieItemLoad | MovieItemSuccess | MovieItemError;
