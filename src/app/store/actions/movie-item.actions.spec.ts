import {MovieItemActionTypes, MovieItemError, MovieItemLoad, MovieItemSuccess} from './movie-item.actions';
import {genreType} from '../../models/genre-type.model';

describe('MovieItem Actions', () => {
  describe('MovieItemLoad Action', () => {
    it('should create an action', () => {
      const payload = {id: 1};
      const action = new MovieItemLoad(payload);

      expect({...action}).toEqual({
        type: MovieItemActionTypes.MOVIE_ITEM_LOAD,
        payload
      });
    });
  });

  describe('MovieItemSuccess Action', () => {
    it('should create an action', () => {
      const payload = {
        movie: {
          id: 20,
          key: 'the-dark-knight',
          name: 'The Dark Knight',
          description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
          genres: [genreType.action, genreType.crime, genreType.drama],
          rate: 9.0,
          length: '2hr 32mins',
          img: 'the-dark-knight.jpg'
        }
      };
      const action = new MovieItemSuccess(payload);

      expect({...action}).toEqual({
        type: MovieItemActionTypes.MOVIE_ITEM_SUCCESS,
        payload
      });
    });
  });

  describe('MovieItemError Action', () => {
    it('should create an action', () => {
      const payload = {error: {name: 'Error name', message: 'Error message', stack: 'Error stack'}};
      const action = new MovieItemError(payload);

      expect({...action}).toEqual({
        type: MovieItemActionTypes.MOVIE_ITEM_ERROR,
        payload
      });
    });
  });
});
