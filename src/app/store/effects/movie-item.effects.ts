import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {MovieService} from '../../services/movie.service';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {catchError, map, switchMap} from 'rxjs/operators';
import {MovieItemActionTypes, MovieItemError, MovieItemLoad, MovieItemSuccess} from '../actions/movie-item.actions';

@Injectable()
export class MovieItemEffects {

  constructor(private actions$: Actions, private movieService: MovieService) {
  }

  @Effect()
  public loadMovieItem$: Observable<Action> = this.actions$
    .pipe(
      ofType<MovieItemLoad>(MovieItemActionTypes.MOVIE_ITEM_LOAD),
      switchMap(
        (action) => this.movieService.get(action.payload.id)
          .pipe(
            map(movie => new MovieItemSuccess({movie})),
            catchError((error) => of(new MovieItemError({error})))
          )
      )
    );
}
