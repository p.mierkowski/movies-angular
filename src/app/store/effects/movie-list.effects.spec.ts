import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable, of, ReplaySubject} from 'rxjs';

import {MovieListEffects} from './movie-list.effects';
import {Movie} from '../../models/movie.model';
import {GenreType, genreType} from '../../models/genre-type.model';
import {MovieService} from '../../services/movie.service';
import {MovieListLoad, MovieListSuccess} from '../actions/movie-list.actions';

class MockMovieService {
  public static mockMovieData: Movie = {
    id: 20,
    key: 'the-dark-knight',
    name: 'The Dark Knight',
    description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
    genres: [genreType.action, genreType.crime, genreType.drama],
    rate: 9.0,
    length: '2hr 32mins',
    img: 'the-dark-knight.jpg'
  };

  findByQuery(query: string, genre?: GenreType): Observable<Movie[]> {
    return of([MockMovieService.mockMovieData]);
  }
}


describe('MovieListEffects', () => {
  let actions$: ReplaySubject<any>;
  let effects: MovieListEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MovieListEffects,
        provideMockActions(() => actions$),
        {provide: MovieService, useClass: MockMovieService}
      ]
    });

    effects = TestBed.get(MovieListEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should return correct answer and type', () => {
    actions$ = new ReplaySubject(1);
    actions$.next(new MovieListLoad({query: 'q', genre: 'action'}));

    effects.loadMovieList$.subscribe(result => {
      expect(result).toEqual(new MovieListSuccess({movies: [MockMovieService.mockMovieData]}));
    });
  });
});
