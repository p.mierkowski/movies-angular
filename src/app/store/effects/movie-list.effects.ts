import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {MovieService} from '../../services/movie.service';
import {catchError, map, mergeMap, switchMap} from 'rxjs/operators';
import {Observable, of} from 'rxjs';
import {MovieListLoad, MovieListSuccess, MovieListActionTypes, MovieListError} from '../actions/movie-list.actions';
import {Action} from '@ngrx/store';

@Injectable()
export class MovieListEffects {

  constructor(private actions$: Actions, private movieService: MovieService) {
  }

  @Effect()
  public loadMovieList$: Observable<Action> = this.actions$
    .pipe(
      ofType<MovieListLoad>(MovieListActionTypes.MOVIE_LIST_LOAD),
      switchMap(
        (action) => this.movieService.findByQuery(action.payload.query, action.payload.genre)
          .pipe(
            map(movies => new MovieListSuccess({movies})),
            catchError((error) => of(new MovieListError({error})))
          )
      )
    );
}
