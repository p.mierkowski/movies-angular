import {Movie} from '../../models/movie.model';
import {MovieListActions, MovieListActionTypes} from '../actions/movie-list.actions';

export interface MovieListState {
  collection: Movie[];
  isLoading: boolean;
  error: Error | null;
}

export const initialState: MovieListState = {
  collection: [],
  isLoading: false,
  error: null
};

export function reducer(state = initialState, action: MovieListActions): MovieListState {
  switch (action.type) {
    case MovieListActionTypes.MOVIE_LIST_LOAD: {
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    }
    case MovieListActionTypes.MOVIE_LIST_SUCCESS: {
      return {
        ...state,
        collection: action.payload.movies,
        error: null,
        isLoading: false
      };
    }
    case MovieListActionTypes.MOVIE_LIST_ERROR: {
      return {
        ...state,
        error: action.payload.error,
        isLoading: false
      };
    }
    default:
      return state;
  }
}
