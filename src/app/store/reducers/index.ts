import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import {environment} from '../../../environments/environment';
import * as fromMovieList from './movie-list.reducer';
import * as fromMovieItem from './movie-item.reducer';

export interface State {
  movieList: fromMovieList.MovieListState;
  movieItem: fromMovieItem.MovieItemState;
}

export const reducers: ActionReducerMap<State> = {
  movieList: fromMovieList.reducer,
  movieItem: fromMovieItem.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];


