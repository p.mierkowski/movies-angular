import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Movie} from '../models/movie.model';
import {GenreType} from '../models/genre-type.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private readonly API_URL = 'http://localhost:3000/';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.API_URL + 'movie?_sort=name');
  }

  public get(id: number): Observable<Movie> {
    return this.http.get<Movie>(this.API_URL + 'movie/' + id);
  }

  public findByQuery(query: string, genre?: GenreType): Observable<Movie[]> {
    let params = new HttpParams();
    params = params.set('_sort', 'name');

    if (query) {
      params = params.set('q', query);
    }

    if (genre) {
      params = params.set('genres_like', genre);
    }
    return this.http.get<Movie[]>(this.API_URL + 'movie', {params});
  }
}
