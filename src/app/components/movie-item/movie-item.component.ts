import {Component, OnInit} from '@angular/core';
import {MovieService} from '../../services/movie.service';
import {ActivatedRoute} from '@angular/router';
import {Movie} from '../../models/movie.model';
import {Title} from '@angular/platform-browser';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {MovieItemState} from '../../store/reducers/movie-item.reducer';
import {MovieItemLoad} from '../../store/actions/movie-item.actions';

@Component({
  selector: 'app-movie-item',
  templateUrl: './movie-item.component.html',
  styleUrls: ['./movie-item.component.sass']
})
export class MovieItemComponent implements OnInit {
  private movieId: number = Number.parseInt(this.route.snapshot.paramMap.get('id'), 10);

  public movie$: Observable<Movie> = this.store.select(state => state.movieItem.item);
  public isLoading$: Observable<boolean> = this.store.select(state => state.movieItem.isLoading);
  public error$: Observable<any> = this.store.select(state => state.movieItem.error);

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private title: Title,
    private readonly store: Store<{ movieItem: MovieItemState }>
  ) {
  }

  ngOnInit() {
    this.store.dispatch(new MovieItemLoad({id: this.movieId}));
    this.setTitle();
  }

  private setTitle() {
    this.movie$.subscribe(movie => {
      if (movie) {
        this.title.setTitle('Movie: ' + movie.name);
      }
    });
  }
}
