import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MovieListComponent} from './movie-list.component';
import {FormsModule} from '@angular/forms';
import {GenreBadgeComponent} from '../genre-badge/genre-badge.component';
import {SpinnerComponent} from '../spinner/spinner.component';
import {RouterModule} from '@angular/router';
import {Movie} from '../../models/movie.model';
import {genreType} from '../../models/genre-type.model';
import {AppRoutingModule} from '../../app-routing.module';
import {MovieItemComponent} from '../movie-item/movie-item.component';
import {ErrorMessageComponent} from '../error-message/error-message.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {HttpClientModule} from '@angular/common/http';
import {Store} from '@ngrx/store';

describe('MovieListComponent', () => {
  const mockMovieData: Movie = {
    id: 20,
    key: 'the-dark-knight',
    name: 'The Dark Knight',
    description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
    genres: [genreType.action, genreType.crime, genreType.drama],
    rate: 9.0,
    length: '2hr 32mins',
    img: 'the-dark-knight.jpg'
  };

  let component: MovieListComponent;
  let fixture: ComponentFixture<MovieListComponent>;
  let store: MockStore<{
    movieList: {
      collection: Movie[];
      isLoading: boolean;
      error: Error | null;
    }
  }>;
  const initialState = {
    movieList: {
      collection: [],
      isLoading: false,
      error: null
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MovieListComponent,
        GenreBadgeComponent,
        SpinnerComponent,
        MovieItemComponent,
        ErrorMessageComponent
      ],
      imports: [
        FormsModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule
      ],
      providers: [
        provideMockStore({initialState})
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.get(Store);

    fixture = TestBed.createComponent(MovieListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render movie list correct', () => {
    store.setState({
      movieList: {
        collection: [mockMovieData],
        isLoading: false,
        error: null
      }
    });

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('div.row .card .card-body h3').textContent).toContain(mockMovieData.name);
    expect(compiled.querySelector('div.row .card .card-body div.text-muted').textContent).toContain(mockMovieData.length);
    expect(compiled.querySelector('div.row .card .card-body p.card-text').textContent).toContain(mockMovieData.description);
  });

  it('should show loading spinner when data are not ready', () => {
    store.setState({
      movieList: {
        collection: [],
        isLoading: true,
        error: null
      }
    });

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('app-spinner .spinner-border .sr-only').textContent).toContain('Loading...');
  });

  it('should show error message when ', () => {
    const expectedErrorMessage = 'Error message';

    store.setState({
      movieList: {
        collection: [],
        isLoading: true,
        error: {
          message: expectedErrorMessage,
          name: 'Error name',
          stack: 'Error stack'
        }
      }
    });

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('app-error-message .alert-warning').textContent).toContain(expectedErrorMessage);
  });
});
