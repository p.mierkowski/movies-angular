import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MovieService} from '../../services/movie.service';
import {Movie} from '../../models/movie.model';
import {ActivatedRoute, Router} from '@angular/router';
import {GenreType} from '../../models/genre-type.model';
import {NgxUiLoaderService} from 'ngx-ui-loader';
import {Title} from '@angular/platform-browser';
import {fromEvent, Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {MovieListLoad} from '../../store/actions/movie-list.actions';
import {MovieListState} from '../../store/reducers/movie-list.reducer';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.sass']
})
export class MovieListComponent implements OnInit {
  @ViewChild('searchQuery')
  public searchQuery: ElementRef;

  public query = this.route.snapshot.queryParamMap.get('q');
  public genre: GenreType = this.route.snapshot.paramMap.get('genre') as GenreType;

  public movies$: Observable<Movie[]> = this.store.select(state => state.movieList.collection);
  public isLoading$: Observable<boolean> = this.store.select(state => state.movieList.isLoading);
  public error$: Observable<any> = this.store.select(state => state.movieList.error);

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private ngxLoader: NgxUiLoaderService,
    private title: Title,
    private router: Router,
    private readonly store: Store<{ movieList: MovieListState }>
  ) {
  }

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.setTitleWithGenre();

    this.store.dispatch(new MovieListLoad({genre: this.genre, query: this.query}));

    fromEvent(this.searchQuery.nativeElement, 'keyup')
      .pipe(
        map((event: any) => {
          return event.target.value;
        })
        , debounceTime(500)
        , distinctUntilChanged()
      )
      .subscribe((text: string) => {
        this.query = text;
        this.searchMovies(this.query, this.genre);
      });
  }

  private searchMovies(query: string, genre: GenreType) {
    this.store.dispatch(new MovieListLoad({genre, query}));

    this.isLoading$.subscribe(
      value => {
        if (value) {
          this.ngxLoader.startBackground();
        } else {
          this.ngxLoader.stopBackground();
        }
      }
    );
  }

  private setTitleWithGenre() {
    if (this.genre) {
      this.title.setTitle('Movies - ' + this.genre);
    } else {
      this.title.setTitle('Movies');
    }
  }
}
